FROM node:current-alpine as builder

RUN mkdir /app
WORKDIR /app
COPY . /app

RUN npm config set strict-ssl false
RUN npm install
RUN npm install --global @vue/cli
RUN npm run build

FROM nginx:stable-alpine

COPY --from=builder /app/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]